1. Replace your Discord Bot token in token.txt 

2. create an Virtual Env 
  ```
  virtualenv -p python3 venv
  ```

3. Install all dependencies from requirements.txt

    ```
    pip install -r requirements.txt
    ```

3. run main.py
    ```
    python main.py
    ```


3. Bot Name
    ```
    discord Bot#2809
    ```
