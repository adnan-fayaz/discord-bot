
import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def main():
    _dbfile = r"./search.db"
    search_table = """CREATE TABLE IF NOT EXISTS searches (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        keyword text NOT NULL,
        value text NOT NULL UNIQUE,
        createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        searchedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );"""

    conn = create_connection(_dbfile)

    if conn is not None:
        create_table(conn,search_table)
    else:
        print("Error! cannot create teh databse connection!")
    return conn

def create_query(searchItem,conn):
    sql = """INSERT INTO searches(
        keyword,value
    ) VALUES(?,?);"""
    cur = conn.cursor()
    keyword = searchItem['keyword']
    value = searchItem['r_url']
    try:
        cur.execute(sql,(keyword,value))
    except Error as e:
        print(e)
    conn.commit()

# def create_result(resultItem,conn):
#     sql = """INSERT INTO result(
#         search_id,url
#     ) VALUES(?,?);"""
    
#     # _dbfile = r"./search.db"
#     # conn = create_connection(_dbfile)
#     cur = conn.cursor()
#     search_id = resultItem['search_id']
#     url = resultItem['url']
#     # item = (search_id, url)
#     cur.execute(sql,(search_id,url))
#     conn.commit()
#     return cur.lastrowid


def findRecent(conn,keyword):
    if keyword is not None:
        keyword = '%'+keyword+'%'
        cur = conn.cursor()
        payload = cur.execute("SELECT value FROM searches WHERE keyword LIKE ?",(keyword,))
        rows = cur.fetchall()
        COLUMN = 0
        column=[elt[COLUMN] for elt in rows]
        conn.commit();
        return column



if __name__== '__main__':
    main()