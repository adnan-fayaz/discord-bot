import discord
import os
from discord.ext.commands import Bot
from database import *
try:
    from googlesearch import search
except ImportError:
    print(f"Can't find Module")

dataFile = open('token.txt', 'r')
for eachLine in dataFile:
    TOKEN = eachLine.split('=')[1]

client = discord.Client()
bot = Bot(command_prefix='!')
@client.event
async def on_ready():
    print(f"Bot is ready on: {client.user}" )
    game = discord.Game(name="Making a Bot")
    await client.change_presence(activity=game)


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if 'hi' in message.content.lower():
        await message.channel.send("hey")
    elif '!recent' in message.content.lower():
        #call Find in dbfunction
        keyword = message.content.lower().split(' ',)[1]
        urls = findRecent(conn=CONN,keyword=keyword)
        print('Returning from Cache DB')
        print(urls)
        if urls:
            for url in urls:
                await message.channel.send(url)
        else:
            await message.channel.send('Data Not Found!')
    elif '!google' in message.content.lower():
        # Get the values from 
        print('Returning from Google')
        query = message.content.lower().split(' ',1)[1]
        if query is not None:
            for url in search(query, tld='co.in', lang='en', num=5, start=0, stop=5, pause=2):
                payload = {
                    'keyword': query,
                    'r_url' : url
                }
                await message.channel.send(url)
                try:
                    create_query(searchItem=payload,conn=CONN)
                except Error as e:
                    print(e)

@bot.command
async def send(ctx):
    global target_channel
    print('Client messages:: ')
    await client.send(channel, message)

CONN = main()
client.run(TOKEN)